#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

// This's not a sigmoid, it's a Fast Sigmoid, an approximation
float sigmoid(float x) {
  return x / (1 + abs(x));
}

struct Perceptron {
  int inlen;
  double *weights;
  double learn_rate;
};

struct Perceptron Perceptron_new(int len_inputs, double init_learn_rate) {
  len_inputs++; // The last element is the bias
    
  double *weights = (double *)malloc(len_inputs * sizeof(double));
  for (int i = 0; i < len_inputs; i++) {
    weights[i] = (double)rand()/(double)(RAND_MAX/2) - 1;
  }
    
  struct Perceptron p = { len_inputs, weights, init_learn_rate };
  return p;
}

double Perceptron_test(double *inps, struct Perceptron p) {
  double *inputs = (double *)malloc(p.inlen * sizeof(double));
  memcpy(inputs, inps, (p.inlen - 1) * sizeof(int));
  inputs[p.inlen - 1] = 1; // Bias

  double output = 0;
  for (int i = 0; i < p.inlen; i++) {
    output += p.weights[i] * inputs[i];
  }
  free(inputs);
  return sigmoid(output);
}

// TODO: use pointer to Perceptron struct
double Perceptron_train(double *inputs, double target, struct Perceptron p) {
  double error = (target - Perceptron_test(inputs, p)) * p.learn_rate;
  for (int i = 0; i < p.inlen; i++) {
    p.weights[i] += error * inputs[i];
  }
  return error;
}

int main() {
  srand((unsigned int)time(NULL));

  struct Perceptron p = Perceptron_new(3, 0.1);
  printf("Len: %d + bias\n", p.inlen - 1);
  printf("Weights: ");
  for (int i = 0; i < p.inlen; i++) {
    printf("%d ", (int)(p.weights[i]*1000));
  }
  printf("\nLearn Rate: %f\n\n", p.learn_rate);

  double *inputs = (double *)malloc(3 * sizeof(double));
  inputs[0] = 0.1f;
  inputs[1] = 0.4f;
  inputs[2] = 0.9f;

  double target = 1;

  for (int e = 0; e < 100; e++) {
    printf("%d\n", (int)(Perceptron_train(inputs, target, p)*1000));
  }
    
    
  free(inputs);
    
  return 0;
}

