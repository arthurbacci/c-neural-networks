#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

// This's not a sigmoid, it's a Fast Sigmoid, an approximation
double sigmoid(double x) {
  return x / (1 + abs(x));
}


struct MultOutPerceptron {
  int num_in;  // Number of inputs
  int num_out; // Number of outputs
  double *w;    // Weights
  double lr;    // Learn rate
};

/*
  About the weights

  | X0  | X1  | X2  | B  | --> Y0
  | X0  | X1  | X2  | B  | --> Y1
  | X0  | X1  | X2  | B  | --> Y2

  | W00 | W01 | W02 | W03 |
  | W10 | W11 | W12 | W13 |
  | W20 | W21 | W22 | W23 |

  In memory:
  | W00 | W01 | W02 | W03 | W10 | W11 | W12 | W13 | W20 | W21 | W22 | W23 |

  Aritmetic:
  Y0 = X0 * W00 + X1 * W01 + X2 * W02 + B * W03
  Y1 = X0 * W10 + X1 * W11 + X2 * W12 + B * W13
  Y2 = X0 * W20 + X1 * W21 + X2 * W22 + B * W23
*/

struct MultOutPerceptron MultOutPerceptron_new(int in_len, int out_len, float learn_rate) {
  double *weights = malloc(in_len * out_len * sizeof(double));
  for (int i = 0; i < in_len * out_len; i++) {
    weights[i] = (double)rand()/(double)(RAND_MAX/2) - 1; // Random number between -1 and 1
  }

  struct MultOutPerceptron p = { in_len, out_len, weights, learn_rate };
  return p;
}

double *MultOutPerceptron_test(double *inputs, struct MultOutPerceptron p) {
  double *outputs = calloc(p.num_out, sizeof(double)); // Needs to all be 0
  for (int y = 0; y < p.num_out; y++) {
    for (int x = 0; x < p.num_in; x++) {
      outputs[y] += p.w[y * p.num_in + x] * inputs[x];
    }
    outputs[y] = sigmoid(outputs[y]);
  }
  return outputs;
}

// Returns the error times the learning rate
double *MultOutPerceptron_train(double *inputs, double *targets, struct MultOutPerceptron *p) {
  double *outputs = MultOutPerceptron_test(inputs, *p);
  double *error = malloc(p->num_out * sizeof(double));
  for (int i = 0; i < p->num_out; i++) {
    error[i] = (targets[i] - outputs[i]) * p->lr;
  }
  free(outputs);

  for (int y = 0; y < p->num_out; y++) {
    for (int x = 0; x < p->num_in; x++) {
      p->w[y * p->num_in + x] += error[y] * inputs[x];
    }
  }
  return error;
}

void MultOutPerceptron_destroy(struct MultOutPerceptron *p) {
  free(p->w);
}

double mean(double *a, int len, char f) {
  double result = 0;
  for (int i = 0; i < len; i++) {
    result += a[i];
  }
  if (f) {
    free(a);
  }
  return result / len;
}

int main() {
  srand((unsigned int)time(NULL));
  
  double inputs[4][3] =  {
    {1, 2, 3},
    {2, 3, 4},
    {4, 3, 2},
    {3, 2, 1}
  };
  double targets[4][2] = {
    {0.1, 0.6},
    {0.1, 0.6},
    {0.6, 0.1},
    {0.6, 0.1}
  };
  
  struct MultOutPerceptron p = MultOutPerceptron_new(3, 2, 0.1);

  clock_t start = clock();
  
  for (int e = 0; e < 10000000; e++) {
    for (int i = 0; i < 4; i++) {
      free(MultOutPerceptron_train(inputs[i], targets[i], &p));
    }
    if (e % 1000000 == 0) {
      printf("%d/10\n", e / 1000000);
    }
  }
  printf("Error: %f;\n", mean(MultOutPerceptron_train(inputs[0], targets[0], &p), p.num_out, 1));
  puts("That wasn't an error message, it's the error variable");

  printf("Time: %ld\n", clock() - start);
  
  MultOutPerceptron_destroy(&p);
  
  return 0;
}

